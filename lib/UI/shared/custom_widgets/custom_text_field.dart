import 'package:flutter/material.dart';

import '../colors.dart';
import '../utils.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField({
    super.key,
    required this.hintText,
    required this.controller,
    this.Obscure,
    this.contentPaddingLeft,
    this.contentPaddingRight,
    this.contentPaddingTop,
    this.contentPaddingBottom,
    this.textAlign,
    this.textSize,
    this.circularSize,
    this.typeInput,
    this.validator,
    this.prefixIcon,
    this.fontSize,
    this.suffixIcon,
    this.borderColor,
    this.colorHintText,
    this.widthBorder,
    this.minLines,
    this.maxLines,
    this.maxLength,
    this.onChanged,
    this.onTap,
  });
  final String hintText;
  final TextEditingController controller;
  final bool? Obscure;
  final double? contentPaddingLeft;
  final double? contentPaddingRight;
  final double? contentPaddingTop;
  final double? contentPaddingBottom;
  final TextAlign? textAlign;
  final double? textSize;
  final double? circularSize;
  final TextInputType? typeInput;
  final String? Function(String?)? validator;
  final IconButton? prefixIcon;
  final double? fontSize;
  final Widget? suffixIcon;
  final Color? borderColor;
  final Color? colorHintText;
  final double? widthBorder;
  final int? minLines;
  final int? maxLines;
  final int? maxLength;
  final ValueChanged<String>? onChanged;
  final VoidCallback? onTap;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onTap: widget.onTap ?? null,
      onChanged: widget.onChanged ?? null,
      minLines: widget.minLines ?? null,
      maxLines: widget.maxLines ?? 1,
      maxLength: widget.maxLength ?? null,
      validator: widget.validator,
      textInputAction: TextInputAction.next,
      keyboardType: widget.typeInput ?? TextInputType.text,
      textAlign: widget.textAlign ?? TextAlign.start,
      obscureText: widget.Obscure ?? false,
      controller: widget.controller,
      style: TextStyle(
        fontSize: widget.textSize ?? screenWidth(25),
        color: AppColors.mainBlackColor,
        fontWeight: FontWeight.normal,
      ),
      decoration: InputDecoration(
          suffixIcon: widget.suffixIcon ?? null,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: widget.widthBorder ?? 1,
                  color: widget.borderColor ?? AppColors.mainGrey2Color),
              borderRadius: BorderRadius.all(
                  Radius.circular(widget.circularSize ?? screenWidth(10)))),
          //suffixIconColor: AppColors.mainYellowColor,
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: widget.widthBorder ?? 1,
                  color: AppColors.mainBlueColor),
              borderRadius: BorderRadius.all(
                  Radius.circular(widget.circularSize ?? screenWidth(10)))),
          prefixIcon: widget.prefixIcon ?? null,
          errorStyle: TextStyle(color: AppColors.mainOrangeColor),
          hintText: widget.hintText,
          hintStyle: TextStyle(
              color: widget.colorHintText ?? AppColors.mainGreyColor,
              fontSize: widget.fontSize ?? screenWidth(30)),
          contentPadding: EdgeInsetsDirectional.only(
              start: widget.contentPaddingLeft ?? 0,
              end: widget.contentPaddingRight ?? 0,
              top: widget.contentPaddingTop ?? 0,
              bottom: widget.contentPaddingBottom ?? 0),
          border: OutlineInputBorder(
              borderSide: BorderSide(
                width: widget.widthBorder ?? 1,
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(widget.circularSize ?? screenWidth(10))))),
    );
  }
}
