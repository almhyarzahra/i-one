import 'package:com.almhyar/UI/shared/custom_widgets/custom_toast.dart';
import 'package:com.almhyar/core/data/models/apis/short_url_model.dart';
import 'package:com.almhyar/core/data/repositories/user_repository.dart';
import 'package:com.almhyar/core/enums/message_type.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../core/services/base_controller.dart';

class HomeController extends BaseController {
  TextEditingController urlController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  Rx<ShortUrlModel> urlModel = ShortUrlModel().obs;

  void shortUrl() async {
    if (isNumeric(urlController.text)) {
      if (formKey.currentState!.validate()) {
        final Uri _url =
            Uri.parse("https://xad.icu/${urlController.text.toString()}");
        if (!await launchUrl(
          _url,
        )) ;
      }
    } else {
      if (formKey.currentState!.validate())
        runFullLoadingFutureFunction(
            function: UserRepository()
                .shortUrl(
                    username: "xad",
                    password: "pass1",
                    action: "shorturl",
                    format: "json",
                    url: urlController.text.trim().toString())
                .then((value) {
          value.fold((l) {
            if (l == null) {
              CustomToast.showMessage(
                  message: "Please Try Again",
                  messageType: MessageType.REJECTED);
            } else {
              urlModel.value = l;
            }
          }, (r) {
            urlModel.value = r;
          });
        }));
    }
  }

  bool isNumeric(String str) {
    final numericRegx = RegExp(r'^[0-9]+$');
    return numericRegx.hasMatch(str);
  }
}
