import 'package:com.almhyar/UI/shared/custom_widgets/custom_button.dart';
import 'package:com.almhyar/UI/shared/custom_widgets/custom_text_field.dart';
import 'package:com.almhyar/UI/shared/utils.dart';
import 'package:com.almhyar/UI/views/home_view/home_controller.dart';
import 'package:com.almhyar/core/utils/general_util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../shared/colors.dart';
import '../../shared/custom_widgets/custom_text.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  HomeController controller = HomeController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Form(
        key: controller.formKey,
        child: Column(
          children: [
            screenWidth(20).ph,
            Center(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(screenWidth(10)),
                  child: Image.asset(
                    "images/ione.png",
                    width: screenWidth(3),
                  )),
            ),
            screenWidth(20).ph,
            Padding(
              padding:
                  EdgeInsetsDirectional.symmetric(horizontal: screenWidth(20)),
              child: CustomTextField(
                  hintText: "Enter Link",
                  controller: controller.urlController,
                  contentPaddingLeft: screenWidth(10),
                  validator: (value) {
                    return value!.isEmpty ? "Please enter your link" : null;
                  }),
            ),
            screenWidth(20).ph,
            Center(
              child: CustomButton(
                text: "Short Link",
                onPressed: () {
                  controller.shortUrl();
                },
                circularBorder: screenWidth(20),
              ),
            ),
            screenWidth(20).ph,
            Obx(() {
              return controller.urlModel.value.status == null
                  ? SizedBox.shrink()
                  : Column(
                      children: [
                        CustomText(
                          content: "Your New Link :",
                          colorText: AppColors.mainRedColor,
                          fontWeight: FontWeight.bold,
                        ),
                        screenWidth(40).ph,
                        Padding(
                          padding: EdgeInsetsDirectional.symmetric(
                              horizontal: screenWidth(20)),
                          child: SelectableText(
                            controller.urlModel.value.shorturl!,
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                decorationThickness: 1),
                          ),
                        ),
                      ],
                    );
            }),
            Image.asset("images/link.png"),

            //SelectableText("Lorem ipsum...")
          ],
        ),
      ),
    ));
  }
}
