class ShortUrlModel {
  String? status;
  String? code;
  String? message;
  String? errorCode;
  int? statusCode;
  Url? url;
  String? title;
  String? shorturl;

  ShortUrlModel(
      {this.status,
      this.code,
      this.message,
      this.errorCode,
      this.statusCode,
      this.url,
      this.title,
      this.shorturl});

  ShortUrlModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    code = json['code'];
    message = json['message'];
    errorCode = json['errorCode'];
    statusCode = json['statusCode'] is String
        ? int.parse(json['statusCode'])
        : json['statusCode'];
    url = json['url'] != null ? new Url.fromJson(json['url']) : null;
    title = json['title'];
    shorturl = json['shorturl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['code'] = this.code;
    data['message'] = this.message;
    data['errorCode'] = this.errorCode;
    data['statusCode'] = this.statusCode;
    if (this.url != null) {
      data['url'] = this.url!.toJson();
    }
    data['title'] = this.title;
    data['shorturl'] = this.shorturl;
    return data;
  }
}

class Url {
  String? keyword;
  String? url;
  String? title;
  String? date;
  String? ip;

  Url({this.keyword, this.url, this.title, this.date, this.ip});

  Url.fromJson(Map<String, dynamic> json) {
    keyword = json['keyword'];
    url = json['url'];
    title = json['title'];
    date = json['date'];
    ip = json['ip'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['keyword'] = this.keyword;
    data['url'] = this.url;
    data['title'] = this.title;
    data['date'] = this.date;
    data['ip'] = this.ip;
    return data;
  }
}
