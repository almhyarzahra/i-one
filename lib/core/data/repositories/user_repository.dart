import 'package:dartz/dartz.dart';

import '../../enums/request_type.dart';
import '../../utils/network_util.dart';
import '../models/apis/short_url_model.dart';
import '../models/common_response.dart';
import '../network/endpoints/user_endpoints.dart';
import '../network/network_config.dart';

class UserRepository {
  Future<Either<ShortUrlModel?, ShortUrlModel>> shortUrl({
    required String username,
    required String password,
    required String action,
    required String format,
    required String url,
  }) async {
    try {
      return NetworkUtil.sendRequest(
        type: RequestType.GET,
        url: UserEndPoints.shortUrl,
        params: {
          "username": username,
          "password": password,
          "action": action,
          "format": format,
          "url": url,
        },
        headers: NetworkConfig.getHeaders(needAuth: false),
      ).then((response) {
        if (response == null) {
          return Left(null);
        }
        CommonResponse<Map<String, dynamic>> commonResponse =
            CommonResponse.fromJson(response);

        if (commonResponse.getStatus) {
          return Right(ShortUrlModel.fromJson(commonResponse.data ?? {}));
        } else {
          return Left(ShortUrlModel.fromJson(commonResponse.data ?? {}));
        }
      });
    } catch (e) {
      return Left(null);
    }
  }
}
